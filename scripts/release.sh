#!/bin/bash

set -x
set -e

release_type=${1}

#version=$(./scripts/version.py get)
#version_file=VERSION
CURRENT_VER='1.6.0'
GITLAB_PROJECT_URL=https://username:glpat-VsEDzUa5mACwWvxbuYVu@gitlab.com/sonnv.phm/release-version-increment.git
#CURRENT_TAG_NAME=$(git describe --tags `git rev-list --tags --max-count=1`)

commit=${CI_COMMIT_SHA:-$(git rev-parse HEAD)}
branch=${ALLOWED_RELEASE_BRANCH:-master}

if ! git branch -a --contains "${commit}" | grep -e "^[* ]*remotes/origin/${branch}\$"
then
  echo -e "###\n### Not on ${branch}. Only ${branch} commits can be released.\n###"
  exit 1
else
  echo -e "###\n### Releasing of ${commit} on ${branch}\n###"
fi

# Do some release stuff here
# Publish docker image or copy to S3 bucket or whatever you need

git config user.name "Son Nguyen"
git config user.email "sonnv.phm@gmail.com"

echo "Pushing detached tag of new version"
# if [ "$CURRENT_TAG_NAME" == "${CURRENT_VER}" ]
# then
#   echo -e "###\n### Current tag version existing with version in VERSION file.\n###"
#   exit 1
#   # git tag -d ${CURRENT_VER}
#   # git push $GITLAB_PROJECT_URL --delete ${CURRENT_VER}
#   # git add ${version_file}
#   # git tag -a ${version} -m "Release version ${version} tag"
#   # git push $GITLAB_PROJECT_URL ${version}
# else

  echo "load latest tag version to VERSION file"
  git fetch --all --tags
  git checkout "${branch}"
  git tag --list
  git describe --tags `git rev-list --tags --max-count=1` > ./VERSION
  git commit -a -m "update VERSION file"
  git push $GITLAB_PROJECT_URL ${branch}

  version=$(./scripts/version.py get)
  version_file=VERSION
  
  git add ${version_file}
  git tag -a ${version} -m "Release version ${version} tag"
  git push $GITLAB_PROJECT_URL ${version}

  echo "Pushing new version to ${branch}"
  git fetch origin "${branch}:${branch}" || git pull
  git checkout "${branch}"

  ./scripts/version.py inc-${release_type}

  next_working_version=$(./scripts/version.py get --with-pre-release-placeholder)
  git add ${version_file}
  git commit -m "Incrementing working version to ${next_working_version} after ${version} release."
  git push $GITLAB_PROJECT_URL ${branch}
#fi
